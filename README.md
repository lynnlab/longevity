# README #

This is a repository for the analysis of the 16S and RNA-Seq data from the Lynn et al. "Differences in the composition of the gut microbiota following early life antibiotic exposure differentially impact host health and longevity in later life." manuscript in Cell Reports.

Analysis is split into 16S rRNA analysis and RNA-Seq analysis.
